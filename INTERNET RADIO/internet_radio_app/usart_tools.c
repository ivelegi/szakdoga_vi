#include "compiler.h"
#include "usart_tools.h"

void usart_initialize(void){
TRISCbits.RC6 = 0; //TX output
TRISCbits.RC7 = 1; //RX input
TXSTAbits.SYNC = 0; //Asynchronous mode
TXSTAbits.TX9 = 0; //8bit transmission
TXSTAbits.BRGH = 1; //Set HIGH Baud rate
TXSTAbits.TXEN = 1; //Enable transmitter
TXSTAbits.SENDB = 0; //Disable break

//RCSTA: Receive Status and Control Register
RCSTAbits.SPEN = 1; //Serial Port enabled
RCSTAbits.RX9 = 0; //8bit reception
RCSTAbits.CREN = 1; //Enables Receiver

//BAUDCON Control register
BAUDCONbits.BRG16 =0;  //16bit baud rate generator
SPBRG =163;           // Set to 9600 baud rate, 12Mhz, High Speed, BRG16

// USART interrupts configuration
//RCONbits.IPEN   = 1; // ENABLE interrupt priority
//INTCONbits.GIE  = 0; // ENABLE interrupts
//INTCONbits.PEIE = 0; // ENable peripheral interrupts.
//PIE1bits.RCIE   = 0; // ENABLE USART receive interrupt
PIE1bits.TXIE   = 0; // disable USART TX interrupt
}

void write_to_usart(rom unsigned char* input)
{
  while (*input !=0)
  {
  while(!TXSTAbits.TRMT);
  TXREG =*input;
  input++;
  }
}
