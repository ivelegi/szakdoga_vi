﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace PortWriter
{
	class Program
	{

		/// <summary>
		/// Writes a message to a specific serial port. To exit from the program, the string "exit" must be entered
		/// </summary>
		
		static void Main(string[] args)
		{
			SerialPort _serialPort = new SerialPort();
			_serialPort.PortName = "com1";
			_serialPort.Open();
			Console.WriteLine("enter string that will written to serial port:");
			string msgToPort ="";
			while (msgToPort !="exit")
			{
				_serialPort.Write(msgToPort);
				msgToPort = Console.ReadLine();
			}
			_serialPort.Close();
		}
	}
}
