﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

namespace Speech_module
{
	class Program
	{
		/// <summary>
		/// It implements the serial communication to the Internet radio board on a specific com port using 9600 baud, 8 bit and no handshake
		/// It acts as a state machine depending on what kind of message arrived. The states are described in an enum below
		/// The communication protocol works as follows:
		/// A start of message '<' character is followed by a message group identifier. It can be individual message (i), menu (m), set value (s).
		/// After this letter, a sequence of characters can arrive which is the identifier of a certain message.
		/// Finally, an end of message (>) character is expected.
		/// </summary>
		enum msgStates {Wait, Command, Menu, Message, SetValue}
		static void Main(string[] args)
		{
					SoundDb db = new SoundDb("../../items.xml");
						SoundManager sm = new SoundManager("../../speechFiles");
						SerialPort _serialPort = new SerialPort();
			_serialPort.PortName ="com2";
						_serialPort.Open();
			string message = "";
			int numBytes;
			int state = (int)msgStates.Wait;
			char currReadChar;
			Console.WriteLine("Speech module started");
			while (message !="iExit;")
			{
				numBytes = _serialPort.BytesToRead;
				if (_serialPort.BytesToRead == 0)
				{
					state =(int)msgStates.Wait;
									}
				else
				{
					currReadChar =(char)_serialPort.ReadByte();
					switch (state)
					{
						case (int) msgStates.Wait:
							if (currReadChar =='<')
							{
								state =(int) msgStates.Command;
																							}
							break;
						case (int)msgStates.Command:
							if (currReadChar =='m')
							{
								state =(int) msgStates.Menu;
																							}
							if (currReadChar =='s')
							{
								state = (int)msgStates.SetValue;
							}
							if (currReadChar == 'i')
							{
								state = (int)msgStates.Message;
															}
							break;
						case (int)msgStates.Menu:
							if (currReadChar =='>')
							{
								sm.stop();
								sm.speakItemWithType(db.getFileNameById(message), db.getFileNameById("Menu"));
								sm.speakMessage(db.getFileNameById("UseUpDownNav"));
																message = "";
							}
														break;
						case (int)msgStates.SetValue:
							if (currReadChar == '>')
							{
								sm.stop();
								sm.speakItemWithType(db.getFileNameById(message), db.getFileNameById("SetValue"));
								sm.speakMessage(db.getFileNameById("UseUpDownMod"));
								message = "";
							}
							break;
						case (int) msgStates.Message:
							if (currReadChar == '>')
							{
								sm.stop();
								sm.speakMessage(db.getFileNameById(message));
								message = "";
							}
							break;
						default:
							state =(int) msgStates.Wait;
							break;
					} //end state handling
					if (currReadChar !='<' &&currReadChar !='>')
										message += currReadChar.ToString();
				} //end char handler
			} //end main inf. loop
						sm.dispose();
		} //end main
	} //end class
} //end namespace
