﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Speech_module
{
/// <summary>
/// A class to provide a data structure for the sound file name-ID assignments
/// </summary>
	class SoundDb
	{
		/// <summary>
		/// A Dictionary to store unique id-file name assignments
		/// </summary>
				private Dictionary<string, string> soundNameAssignments;
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="fileName">The xml file containing the id-file name assignments</param>
		public SoundDb(string fileName)
		{
			soundNameAssignments = new Dictionary<string, string>();
			readKVPStringsFromXml(fileName, soundNameAssignments, "item");
		}
		/// <summary>
		/// Reads an xml's content to a dictionary
		/// </summary>
		/// <param name="fileName">The name of the xml to be read</param>
		/// <param name="dest">The destination dictionary containing the key-value pairs</param>
		/// <param name="elementName">The element name describing an assignment</param>
		public void readKVPStringsFromXml(string fileName, Dictionary<string, string> dest, string elementName)
		{
			XDocument document = null;
			try
			{
				document = XDocument.Load(fileName);
			}
			catch (FileNotFoundException e)
			{
				Console.WriteLine("Error, can't find sound database xml file:" + fileName);
				return;
			}
			#region fetching all the string names and translations 
			var strings = from r in document.Descendants(elementName)
						  select new
						  {
							  Name = r.Attribute("name").Value,
							  Val = r.Value
						  };
			foreach (var s in strings)
			{
				try
				{
					dest.Add(s.Name, s.Val);
				}
				catch (ArgumentException e)
				{
					Console.WriteLine("Error, this entry already exists in the database:" + s.Name);
				}
			} //end loop
			#endregion
			Console.WriteLine("Sound database loaded successfully.");
		} //end method
		/// <summary>
		/// Returns a file name for a given unique id
		/// </summary>
		/// <param name="id">The id of the current item to be spoken</param>
		/// <returns>string</returns>
		public string getFileNameById(string id)
		{
			string fileName;
			try
			{
								fileName = soundNameAssignments[id];
			}
			catch (KeyNotFoundException)
			{
				Console.WriteLine(id +" can't be found");
				fileName = null;
			}
			return fileName;
		} //end method

			} //end claass
} //end namespace