﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NAudio.Wave;

namespace Speech_module
{
	/// <summary>
	/// A class to handle sound playing events by using nAudio library
	/// </summary>
	class SoundManager
	{
		/// <summary>
		///  path to the speech files to be played
		/// </summary>
		private string soundFolderPath;
		/// <summary>
		/// Reference for the player1 object
		/// </summary>
				private IWavePlayer player1;
		/// <summary>
		/// Reference for the player2 object
		/// </summary>
		private IWavePlayer player2;
		/// <summary>
		/// Reference for the player3 object
		/// </summary>
		private IWavePlayer player3;
		/// <summary>
		/// File reader object
		/// </summary>
		private AudioFileReader audio;

		//variable to prevent twice playing of simple messages
		private bool canPlay;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="soundFolderPath">The full path to the folder containing the speech files</param>
		public SoundManager(string soundFolderPath)
		{
			this.soundFolderPath = soundFolderPath;
this.			player1 = new WaveOut(WaveCallbackInfo.FunctionCallback());
			this.player2 = new WaveOut(WaveCallbackInfo.FunctionCallback());
			this.player3 = new WaveOut(WaveCallbackInfo.FunctionCallback());
			this.canPlay = true;
			Console.WriteLine("Sound player object initialised successfully");
		}

		/// <summary>
		/// The event triggered play speak which takes two file name to be played as arguments
		/// It calls the asynchronous play method of IWavePlayer to prevent the caller from waiting to the action to be finished
		/// </summary>
		/// <param name="itemFileName">The file corresponding to the spoken item, e. g. exit, add station.</param>
		/// <param name="typeFileName">The file corresponsds to the item type, e. g. slider, menu.</param>
				public void speakItemWithType(string itemFileName, string typeFileName)
		{
			try
			{
				audio = new AudioFileReader(soundFolderPath + "\\" + itemFileName);
			}
						catch (Exception)
			{
				return;
			}
			
			player1.Init(audio);
							player1.Play();
			canPlay = false;
			try
			{
				audio = new AudioFileReader(soundFolderPath + "\\" + typeFileName);
			}
			catch (Exception)
			{
				return;
			}

			player2.Init(audio);
			player1.PlaybackStopped += (pbss, pbse) =>
			{
								player2.Play();
			};
		} //end method
		/// <summary>
		/// Speak method to play a simple message without further text.
		/// </summary>
		/// <param name="msgFileName">The corresponding file to be played</param>
		public void speakMessage(string msgFileName)
		{
			try
			{
				audio = new AudioFileReader(soundFolderPath + "\\" + msgFileName);
			}
									catch (Exception)
			{
				return;
			}
			player3.Init(audio);
						player2.PlaybackStopped += (pbss, pbse) =>
			{
				player3.Play();
				canPlay = false;
							};
			if (canPlay)
			{
				player3.Play();
			}
								}

		/// <summary>
		/// Method to stop all play events
		/// </summary>
		public void stop()
		{
			canPlay = true;
			player1.Stop();
			player2.Stop();
			//player3.Stop();
		}

		/// <summary>
		/// Method to dispose the player objects
		/// </summary>
		public void dispose()
		{
			player1.Dispose();
			player2.Dispose();
	player1 = null;
			player2 = null;
			audio = null;
}
	} //end class
} //end namespace
